<?php
declare(strict_types=1);

namespace JLanger\TokenGenerator;

use JLanger\Blowfish\Blowfish;
use JLanger\TokenGenerator\Exceptions\DateRangeException;
use function mt_rand;

class TokenGenerator implements TokenGeneratorInterface
{
    private TokenInterface $token;
    
    /** @var string Blowfishkey */
    private string $blowfishKey = '';

    /**
     * @inheritDoc
     */
    public function __construct(TokenInterface $token)
    {
        $this->token = $token;
    }

    /**
     * @inheritDoc
     */
    public function generate(): TokenInterface
    {
        $this->validateDateRange();
            $token = $this->generateRandomString();
            if ($this->blowfishKey) {
                $blowfish = new Blowfish($this->blowfishKey);
                $token = $blowfish->encrypt($token);
            }
            $this->token->setToken($token);
            $this->token->setUseLeft($this->token->getMaxUse());
        return $this->token;
    }

    /**
     * Generates a random String.
     *
     * @return string
     */
    private function generateRandomString(): string
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $token = '';
        for ($i = 0; $i < $this->token->getLength(); $i++) {
            $token .= $characters[mt_rand(0, 61)];
        }
        return $token;
    }

    /**
     * @inheritDoc
     */
    public function setBlowfishKey(string $key): TokenGeneratorInterface
    {
        $this->blowfishKey = $key;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function generateRandomBlowfishKey(): void
    {
        $this->blowfishKey = $this->generateRandomString();
    }

    /**             
     * @throws DateRangeException
     */
    private function validateDateRange(): void
    {
        if ($this->token->getEndDate()->getTimestamp() > 0) {
            if ($this->token->getBeginDate()->getTimestamp() === 0) {
                throw new DateRangeException('begin date must not be timestamp 0 when end date is set.');
            }
            if ($this->token->getBeginDate() > $this->token->getEndDate()) {
                throw new DateRangeException('start date must not be later than end date.');
            }
        }
        if ($this->token->getBeginDate()->format(Token::DATE_FORMAT) < '2020-04-14 00:00:01') {
            throw new DateRangeException('begin date must be after 2020-04-14 00:00:01');
        }
    }
}