<?php
declare(strict_types=1);

namespace JLanger\TokenGenerator;


use JLanger\TokenGenerator\Exceptions\DateRangeException;

interface TokenGeneratorInterface
{
    /**
     * TokenGeneratorInterface constructor.
     *
     * @param TokenInterface $token
     */
    public function __construct(TokenInterface $token);

    /**
     * Generate the Token itself.
     *
     * @return TokenInterface
     *
     * @throws DateRangeException
     */
    public function generate(): TokenInterface;
    
    /**
     * @param string $key
     *
     * @return TokenGeneratorInterface
     */
    public function setBlowfishKey(string $key): TokenGeneratorInterface;
    
    /**
     * Generates a random Blowfish-key. This enforces Blowfish-encryption.
     */
    public function generateRandomBlowfishKey(): void;
}