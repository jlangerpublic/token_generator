<?php
declare(strict_types=1);

namespace JLanger\TokenGenerator;


use DateTime;
use Exception;

interface TokenInterface
{
    /**
     * @param DateTime $date
     *
     * @return $this
     */
    public function setEndDate(DateTime $date): self;
    
    public function getEndDate(): DateTime;

    /**
     * @param DateTime $date
     *
     * @return $this
     */
    public function setBeginDate(DateTime $date): self;
    
    public function getBeginDate(): DateTime;


    /**
     * @param int $maxUse
     *
     * @return $this
     */
    public function setMaxUse(int $maxUse): self;
    
    public function getMaxUse(): int;
    
    /**
     * @param int $useLeft
     *
     * @return $this
     */
    public function setUseLeft(int $useLeft): self;

    /**
     * if $length is lower than 20, $length will be set to 20.
     *
     * @param int $length
     *
     * @return Token
     */
    public function setLength(int $length): self;

    public function getLength(): int;
    /**
     * @param string $token
     *
     * @return $this
     */
    public function setToken(string $token): self;

    /**
     * @return string
     */
    public function getToken(): string;


    /**
     * Validates the Token.
     * @return bool
     * @throws Exception
     */
    public function validate(): bool;

    /**
     * Updates the uses left for the Token.
     * 
     * @return $this
     */
    public function updateUse(): self;
    
}