<?php
declare(strict_types=1);

namespace JLanger\TokenGenerator;


use DateTime;
use Exception;
use function date;
use function strlen;

class Token implements TokenInterface
{
    /** @var string  */
    const DATE_FORMAT = 'Y-m-d H:i:s';
    
    private int $length = 20;
    
    private string $token;
    
    private DateTime $beginDate;
    
    private DateTime $endDate;
    
    private int $maxUse = 0;
    
    private int $useLeft = 0;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->beginDate = new DateTime();
        $this->endDate = new DateTime(date(self::DATE_FORMAT, 0));
    }

    /**
     * @inheritDoc
     */
    public function setEndDate(DateTime $date): self
    {
        $this->endDate = $date;
        
        return $this;
    }
    
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @inheritDoc
     */
    public function setBeginDate(DateTime $date): self
    {
        $this->beginDate = $date;
        
        return $this;
    }
    
    public function getBeginDate(): DateTime
    {
        return $this->beginDate;
    }

    /**
     * @inheritDoc
     */
    public function setMaxUse(int $maxUse): self
    {
        $this->maxUse = $maxUse;
        
        return $this;
    }
    
    public function getMaxUse(): int
    {
        return $this->maxUse;
    }

    /**
     * @inheritDoc
     */
    public function setLength(int $length): self
    {
        $this->length = $length >= 20 ? $length : 20;
        
        return $this;
    }
    
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @inheritDoc
     */
    public function setToken(string $token): self 
    {
        $this->token = $token;
        
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @inheritDoc
     */
    public function validate(): bool
    {
        if (strlen($this->token) !== $this->length) {
            return false;
        }
        
        if ($this->maxUse > 0 && $this->useLeft <= 0) {
            return false;
        }
        
        if ($this->maxUse < 0) {
            return false;
        }

        $now = new DateTime();
        if ($this->beginDate >= $now) {
            return false;
        }

        if ($this->endDate->getTimestamp() > 0) {
            return $this->endDate > $now;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function updateUse(): TokenInterface
    {
        $this->useLeft--;
        
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setUseLeft(int $useLeft): TokenInterface
    {
        $this->useLeft = $useLeft;
        
        return $this;
    }
}