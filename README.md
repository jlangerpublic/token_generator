# token_generator

### **Usage:**

First run ```composer install --no-dev```

#### **Minimal use:**
This generates a random Token (A-Z,a-z,0-9) with length 20. The Token can be used once without a time limit.
```php
require_once '***path-to-vendor-folder***/autoload.php';
use JLanger\TokenGenerator\Token;
use JLanger\TokenGenerator\TokenGenerator;
$token = new Token();
try{
$token->setBeginDate(new DateTime('2020-06-01'))
        ->setEndDate(new DateTime('2020-06-20'))
        ->setMaxUse(0)
        ->setUseLeft(0);
$token = (new TokenGenerator($token))->generate();
} catch (Exception $e) {
    trigger_error(get_class($e) . ': ' . $e->getMessage(), E_USER_ERROR);
}
```

Optional parameters:

use ``->beginDate(dateFormatString)`` to set a time where the token starts to be valid.\
use ``->endDate(dateFormatString)`` to set a date until the token will be valid.\
use ``->setBlowfishKey(string)`` to set the blowfish-key. Setting the key will enforce additional blowfish-encryption of the token.\
use ``->generateBlowfishKey()`` to generate a random blowfish-key. Setting the key will enforce additional blowfish-encryption of the token.\
use ``->setMaxUse(int)`` to specify how often the token can be used. 0 means forever.\
use ``->setLength(int)`` to specify the length of the random generated string for the token. Minimum is 20.

**Default parameters:**\
beginDate: now\
endDate: timestamp 0\
maxUse: 0\
length: 20  
  
  
#### **Validate a Token:**
```php
require_once '***path-to-vendor-folder***/autoload.php';
use JLanger\TokenGenerator\Token;
use JLanger\TokenGenerator\TokenGenerator;
$token = new Token();
try{
    $token->setBeginDate(new DateTime('2020-06-01'))
            ->setEndDate(new DateTime('2020-06-20'))
            ->setMaxUse(10)
            ->setUseLeft(10)
            ->setLength(20)
            ->setToken('176254dFgsr567FFzq67');
    $validate = $token->validate(); /* returns true or false. */
    /* optional */
    $token->updateUse();
} catch (Exception $e) {
    trigger_error(get_class($e) . ': ' . $e->getMessage(), E_USER_ERROR);
}

```